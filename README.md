# PacGUI

Simple, zenity based, Pacman GUI to assist automatic updates on Arch based systems

This program will promt the user once a day for updates based on a systemd timer.

Enable the timer `systemctl enable --user pacgui.timer` and forget about it.

## Features
- It updates the system with little intervention.
- Install it and forget it.
- It does not bloat the system.
- It just updates the system and nothing else. 

## Recommendation

As this script will not ask for user password or credentials, it is recommended to
configure `sudo` to execute the following pacman commands with no user intervention:

- `pacman -Syy --noconfirm` to update the database
- `pacman -Syw --noconfirm` to download the pending updates
- `pacman -Syu --noconfirm` to upgrade the system

A suggested configuration to the user `/etc/sudoers`

```bash
Cmnd_Alias PACMANSYN = /usr/bin/pacman -Syy --noconfirm
Cmnd_Alias PACMANUPD = /usr/bin/pacman -Syw --noconfirm
Cmnd_Alias PACMANDOW = /usr/bin/pacman -Syu --noconfirm

myuser ALL=(root) NOPASSWD: PACMANSYN
myuser ALL=(root) NOPASSWD: PACMANUPD
myuser ALL=(root) NOPASSWD: PACMANDOW
```

In this case the user `myuser` can only execute those specific commands withouth being asked
for a password, but even different pacman commands will promt for a password or fail, in case
the user is not allowed to.

## Screenshots

![](img/status.jpg)
![](img/continue.jpg)
![](img/install.jpg)
![](img/done.jpg)
